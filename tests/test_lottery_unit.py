from brownie import Lottery, accounts, config, network, exceptions
import pytest
from scripts.deploy_lottery import deployLottery
from web3 import Web3

from scripts.helpfulScripts import (
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
    fundWithLink,
    getAccount,
    getContract,
)


def testGetEntranceFee():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()

    lottery = deployLottery()
    entrance_fee = lottery.getEntranceFee()
    expected = Web3.toWei(0.025, "ether")
    assert entrance_fee == expected


def testCantEnterUnlessStarted():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    lottery = deployLottery()
    with pytest.raises(exceptions.VirtualMachineError):
        lottery.enter({"from": getAccount(), "value": lottery.getEntranceFee()})


def testCanStartAndEnterLottery():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    lottery = deployLottery()
    account = getAccount()
    print(account)
    lottery.startLottery({"from": account})
    ethToSend = lottery.getEntranceFee()
    lottery.enter({"from": account, "value": ethToSend})
    assert lottery.players(0) == account


def testCanEndLottery():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    lottery = deployLottery()
    account = getAccount()
    lottery.startLottery({"from": account})
    lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    fundWithLink(lottery)
    lottery.endLottery({"from": account})
    assert lottery.lottery_state() == 2


def testCanPickWinnerCorrectly():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    lottery = deployLottery()
    account = getAccount()
    lottery.startLottery({"from": account})
    lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    lottery.enter({"from": getAccount(index=1), "value": lottery.getEntranceFee()})
    lottery.enter({"from": getAccount(index=2), "value": lottery.getEntranceFee()})
    fundWithLink(lottery)
    starting_balance_of_account = account.balance()
    balance_of_lottery = lottery.balance()
    tx = lottery.endLottery({"from": account})
    requestId = tx.events["requestedRandomness"]["requestId"]
    STATIC_RNG = 777
    getContract("vrf_coordinator").callBackWithRandomness(
        requestId, STATIC_RNG, lottery.address, {"from": account}
    )
    assert lottery.recentWinner() == account
    assert lottery.balance() == 0
    assert account.balance() == starting_balance_of_account + balance_of_lottery
