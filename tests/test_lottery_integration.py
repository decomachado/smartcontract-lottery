import time
from brownie import network
import pytest
from scripts.helpfulScripts import (
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
    fundWithLink,
    getAccount,
)
from scripts.deploy_lottery import deployLottery


def testCanPickWinner():
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    lottery = deployLottery()
    account = getAccount()
    lottery.startLottery({"from": account})
    lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    fundWithLink(lottery)
    lottery.endLottery({"from": account})
    time.sleep(180)
    assert lottery.recentWinner() == account
    assert lottery.balance() == 0
