import time
from brownie import Lottery, accounts, config, network
from scripts.helpfulScripts import fundWithLink, getAccount, getContract


def deployLottery():
    account = getAccount()
    print(account)
    lottery = Lottery.deploy(
        getContract("eth_to_usd_price_feed").address,
        getContract("vrf_coordinator").address,
        getContract("link_token").address,
        config["networks"][network.show_active()]["fee"],
        config["networks"][network.show_active()]["keyhash"],
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify", False),
    )
    print("Deployed Lottery")
    return lottery


def startLottery():
    account = getAccount()
    lottery = Lottery[-1]
    starting_tx = lottery.startLottery({"from": account})
    starting_tx.wait(1)
    print("Lottery has started")


def enterLottery():
    account = getAccount()
    lottery = Lottery[-1]
    value = lottery.getEntranceFee() + 100000000
    tx = lottery.enter({"from": account, "value": value})
    tx.wait(1)
    print("You entered the lottery")


def endLottery():
    account = getAccount()
    lottery = Lottery[-1]
    tx = fundWithLink(lottery.address)
    tx.wait(1)
    ending_tx = lottery.endLottery({"from": account})
    ending_tx.wait(1)
    time.sleep(180)
    print(f"{lottery.recentWinner()} is the new winner")


def main():
    deployLottery()
    startLottery()
    enterLottery()
    endLottery()
